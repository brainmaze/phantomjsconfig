рекурсер:
```bash
docker network create dockernet

docker run -d \
       --restart="always" \
       --name dnsrecursor \
       -p 0.0.0.0:53:53 -p 0.0.0.0:53:53/udp \
       antage/pdns-recursor \
            --daemon=no \
            --local-address=0.0.0.0 \
            --forward-zones=.=127.0.0.11 \
            --max-cache-ttl=30

docker network connect dockernet dnsrecursor
```